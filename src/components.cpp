#include <stream9/urls/components.hpp>

#include <stream9/string_view.hpp>

namespace stream9::urls {

string_view
serialize(components const& c)
{
    auto* begin = c.scheme.begin();
    auto* end = c.scheme.end();

    if (!c.user_info.empty()) {
        end = c.user_info.end();
    }
    if (!c.host.empty()) {
        end = c.host.end();
    }
    if (!c.port.empty()) {
        end = c.port.end();
    }
    if (!c.path.empty()) {
        end = c.path.end();
    }
    if (!c.query.empty()) {
        end = c.query.end();
    }
    if (!c.fragment.empty()) {
        end = c.fragment.end();
    }

    return { begin, static_cast<size_t>(end - begin) };
}

} // namespace stream9::urls
