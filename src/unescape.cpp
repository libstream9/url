#include <stream9/urls/unescape.hpp>

#include <stream9/errors.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

namespace stream9::urls {

static int
hex_to_int(char c)
{
    if ('0' <= c && c <= '9') {
        return c - '0';
    }
    else if ('A' <= c && c <= 'Z') {
        return 10 + c - 'A';
    }
    else if ('a' <= c && c <= 'z') {
        return 10 + c - 'a';
    }
    else {
        throw_error(errc::invalid_argument);
    }
}

string
unescape(string_view s)
{
    try {
        string result;
        result.reserve(s.size());

        for (auto it = s.begin(); it != s.end();) {
            auto c = *it;
            if (c != '%') {
                result.append(c);
                ++it;
            }
            else {
                int decoded;

                ++it;
                decoded = hex_to_int(*it) << 4;

                ++it;
                decoded |= hex_to_int(*it);

                result.append(static_cast<char>(decoded));
                ++it;
            }
        }
        return result;
    }
    catch (...) {
        rethrow_error({ { "s", s }, });
    }
}

} // namespace stream9::urls
