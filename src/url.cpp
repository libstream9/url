#include <stream9/url.hpp>

#include <stream9/urls/components.hpp>
#include <stream9/urls/parser.hpp>

#include <ostream>

#include <stream9/errors.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

namespace stream9 {

template<typename Fn>
string_view
parse_component(string_view view, Fn& h)
{
    urls::parse(view, h);

    return h.result;
}

// url

url::
url(string_view str)
{
    try {
        urls::parse(str);
        m_str = str;
    }
    catch (...) {
        rethrow_error();
    }
}

url& url::
operator=(string_view s)
{
    try {
        urls::parse(s);
        m_str = s;

        return *this;
    }
    catch (...) {
        rethrow_error();
    }
}

string_view url::
scheme() const noexcept
{
    struct handler : urls::event_handler {
        string_view result = "";
        void on_scheme(string_view v) override { result = v; }
    } h;

    return parse_component(m_str, h);
}

string_view url::
user_info() const noexcept
{
    struct handler : urls::event_handler {
        string_view result = "";
        void on_user_info(string_view v) override { result = v; }
    } h;

    return parse_component(m_str, h);
}

string_view url::
host() const noexcept
{
    struct handler : urls::event_handler {
        string_view result = "";
        void on_host(string_view v) override { result = v; }
    } h;

    return parse_component(m_str, h);
}

string_view url::
port() const noexcept
{
    struct handler : urls::event_handler {
        string_view result = "";
        void on_port(string_view v) override { result = v; }
    } h;

    return parse_component(m_str, h);
}

string_view url::
path() const noexcept
{
    struct handler : urls::event_handler {
        string_view result = "";
        void on_path(string_view v) override { result = v; }
    } h;

    return parse_component(m_str, h);
}

string_view url::
query() const noexcept
{
    struct handler : urls::event_handler {
        string_view result = "";
        void on_query(string_view v) override { result = v; }
    } h;

    return parse_component(m_str, h);
}

string_view url::
fragment() const noexcept
{
    struct handler : urls::event_handler {
        string_view result = "";
        void on_fragment(string_view v) override { result = v; }
    } h;

    return parse_component(m_str, h);
}

urls::components url::
components() const noexcept
{
    return urls::parse(m_str);
}

std::ostream&
operator<<(std::ostream& oss, url const& u)
{
    try {
        oss << u.m_str;
        return oss;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9
