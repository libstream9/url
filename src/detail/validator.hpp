#ifndef STREAM9_URL_DETAIL_VALIDATOR_HPP
#define STREAM9_URL_DETAIL_VALIDATOR_HPP

#include <stream9/urls/validator.hpp>

#include <stream9/string_view.hpp>

#include <functional>

namespace stream9::urls::validator {

using iterator = string_view::iterator;
using error_handler_t = std::function<bool(iterator&, validation_errc)>;

inline bool
null_error_handler(iterator&, validation_errc)
{
    return true; // stop validation
}

/*
 * classifier
 */
bool is_alpha(char c);
bool is_digit(char c);
bool is_hexdig(char c);
bool is_unreserved(char c);
bool is_sub_delims(char c);

/*
 * non-terminal
 */
bool unreserved(iterator& begin, iterator end, error_handler_t);
bool hexdig(iterator& begin, iterator end, error_handler_t);
bool sub_delims(iterator& begin, iterator end, error_handler_t);
bool pct_encoded(iterator& begin, iterator end, error_handler_t);
bool dec_octet(iterator& begin, iterator end, error_handler_t);
bool pchar(iterator& begin, iterator end, error_handler_t);
bool segment(iterator& begin, iterator end, error_handler_t);
bool segment_nz(iterator& begin, iterator end, error_handler_t);
bool ipv4_address(iterator& begin, iterator end, error_handler_t);
bool reg_name(iterator& begin, iterator end, error_handler_t);
bool ipvfuture(iterator& begin, iterator end, error_handler_t);
bool h16(iterator& begin, iterator end, error_handler_t);
bool ls32(iterator& begin, iterator end, error_handler_t);
bool ipv6_address(iterator& begin, iterator end, error_handler_t);
bool ip_literal(iterator& begin, iterator end, error_handler_t);

/*
 * URI component
 */
bool scheme(string_view, error_handler_t = null_error_handler);
bool user_info(string_view, error_handler_t = null_error_handler);
bool host(string_view, error_handler_t = null_error_handler);
bool port(string_view, error_handler_t = null_error_handler);
bool path_abempty(string_view, error_handler_t = null_error_handler);
bool path_absolute(string_view, error_handler_t = null_error_handler);
bool path_rootless(string_view, error_handler_t = null_error_handler);
bool path_empty(string_view, error_handler_t = null_error_handler);
bool query(string_view, error_handler_t = null_error_handler);
bool fragment(string_view, error_handler_t = null_error_handler);

} // namespace stream9::urls::validator

#endif // STREAM9_URL_DETAIL_VALIDATOR_HPP
