#include <stream9/urls/parser.hpp>

#include <stream9/errors.hpp>
#include <stream9/strings/stream.hpp>
#include <stream9/strings/starts_with.hpp>

#include <string>

// URL       = scheme ":" [hier-part] ["?" query] ["#" fragment]
//
// scheme    = <any OCTET except ":" / "@" / "/" / "?" / "#">
//           ; ":" indicate end of scheme
//           ; "@" indicate error that user_info start without scheme
//           ; "/" indicate error that path start without scheme
//           ; "?" indicate error that path start without scheme
//           ; "#" indicate error that path start without scheme
//
// hier-part = "//" authority ["/" path]
//           / ["/"] path
//
// authority = [user_info "@"] host [":" port]
//
// user_info = *<any OCTET except "@" / "/" / "?" / "#">
//           ; "@" indicate end of user_info
//           ; "/" indicate begining of path
//           ; "?" indicate that parser was looking at path
//           ; "#" indicate that parser was looking at path
//
// host      = *<any OCTET except ":" / "/" / "?" / "#">
//           ; ":" indicate begining of port
//           ; "/" indicate begining of path
//           ; "?" indicate that parser was looking at path
//           ; "#" indicate that parser was looking at path
//
// port      = *DIGIT
//
// path      = *<any OCTET except "?" / "#">
//           ; "?" indicate begining of query
//           ; "#" indicate begining of fragment
//
// query     = *<any OCTET except "#">
//           ; "#" indicate begining of fragment
//
// fragment  = *OCTET

namespace stream9::urls {

using iterator = string_view::const_iterator;

static auto
make_string_view(iterator b, iterator e)
{
    assert(b <= e);
    auto len = static_cast<size_t>(e - b);
    return string_view(b, len);
}

static bool
starts_with(iterator b, iterator e, string_view rhs)
{
    using stream9::strings::starts_with;

    return starts_with(string_view(b, e), rhs);
}

static bool
fragment(iterator& it, iterator e, event_handler& h)
{
    if (it == e || *it != '#') [[likely]] return true;

    h.on_fragment(make_string_view(++it, e));

    return true;
}

static bool
query(iterator& it, iterator e, event_handler& h)
{
    if (it == e || *it != '?') [[likely]] return true;

    auto start = ++it;
    for (; it != e; ++it) {
        if (*it == '#') [[unlikely]] break;
    }

    h.on_query(make_string_view(start, it));

    return true;
}

static bool
path(iterator& it, iterator e, event_handler& h)
{
    auto is_separator = [](auto c) { return c == '?' || c == '#'; };

    auto start = it;
    for (; it != e; ++it) {
        if (is_separator(*it)) [[unlikely]] break;
    }

    h.on_path(make_string_view(start, it));
    return true;
}

static bool
port(iterator& it, iterator e, event_handler& h)
{
    auto is_valid = [](auto c) {
        return '0' <= c && c <= '9';
    };

    if (it == e) [[unlikely]] return true;
    if (*it != ':') [[likely]] return true;

    auto start = ++it; // skip ':'
    for (; it != e; ++it) {
        if (!is_valid(*it)) [[unlikely]] break;
    }

    h.on_port(make_string_view(start, it));
    return true;
}

static bool
host(iterator& it, iterator e, event_handler& h)
{
    auto is_separator = [](auto c) {
        return c == ':' || c == '/' || c == '?' || c == '#';
    };

    auto start = it;
    for (; it != e; ++it) {
        if (is_separator(*it)) [[unlikely]] break;
    }

    h.on_host(make_string_view(start, it));
    return true;
}

static bool
user_info(iterator& it, iterator e, event_handler& h)
{
    auto is_valid = [](auto c) {
        return c != '/' && c != '?' && c != '#';
    };

    auto j = it;
    for (; j != e; ++j) {
        if (*j == '/') return true;
        if (*j == '@') break;
    }
    if (j == e) [[likely]] return true;
    auto end = j;

    auto start = it;
    for (; it != end; ++it) {
        if (!is_valid(*it)) [[unlikely]] {
            h.on_error(parse_errc::invalid_char);
            return false;
        }
    }

    h.on_user_info(make_string_view(start, it));
    ++it; // skip '@'

    return true;
}

static bool
authority(iterator& s, iterator e, event_handler& h)
{
    if (!starts_with(s, e, "//")) [[unlikely]] return true;

    s += 2; // skip '//'

    if (!user_info(s, e, h)) [[unlikely]] return false;

    if (!host(s, e, h)) [[unlikely]] return false;

    return port(s, e, h);
}

static bool
hier_part(iterator& s, iterator e, event_handler& h)
{
    if (!authority(s, e, h)) [[unlikely]] return false;

    return path(s, e, h);
}

static bool
scheme(iterator& it, iterator e, event_handler& h)
{
    auto constexpr delimiter = ':';

    auto is_valid = [](auto c) {
        return c != '@' && c != '/' && c != '?' && c != '#';
    };

    auto start = it;
    for (; it != e; ++it) {
        auto ch = *it;

        if (ch == delimiter) [[unlikely]] {
            h.on_scheme(make_string_view(start, it));
            ++it; // skip delimiter
            return true;
        }
        else if (!is_valid(ch)) [[unlikely]] {
            h.on_error(parse_errc::invalid_char);
            return false;
        }
    }

    h.on_error(parse_errc::no_scheme);

    return false;
}

bool
parse(iterator s, iterator end, event_handler& h)
{
    if (!scheme(s, end, h)) [[unlikely]] return false;

    if (!hier_part(s, end, h)) [[unlikely]] return false;

    if (!query(s, end, h)) [[unlikely]] return false;

    if (!fragment(s, end, h)) [[unlikely]] return false;

    return true;
}

bool
parse(string_view s, event_handler& h)
{
    auto it = s.begin();

    return parse(it, s.end(), h);
}

class handler : public event_handler
{
public:
    handler(components& components) : m_result { components } {}

    void on_scheme(string_view s) override
    {
        m_result.scheme = s;
    }

    void on_user_info(string_view s) override
    {
        m_result.user_info = s;
    }

    void on_host(string_view s) override
    {
        m_result.host = s;
    }

    void on_port(string_view s) override
    {
        m_result.port = s;
    }

    void on_path(string_view s) override
    {
        m_result.path = s;
    }

    void on_query(string_view s) override
    {
        m_result.query = s;
    }

    void on_fragment(string_view s) override
    {
        m_result.fragment = s;
    }

    void on_error(parse_errc ec) override
    {
        m_error = ec;
    }

    parse_errc error() const { return m_error; }

private:
    components& m_result;
    parse_errc m_error = parse_errc::ok;
};

components
parse(string_view input)
{
    components result;
    handler h { result };

    auto s = input.begin();

    if (!parse(s, input.end(), h)) [[unlikely]] {
        auto loc = s - input.begin();
        throw stream9::error {
            h.error(), {
                { "url", input },
                { "location", loc },
            }
        };
    }

    return result;
}

std::error_category const&
parse_error()
{
    static struct impl : std::error_category {
        char const* name() const noexcept
        {
            return "parse_error";
        }

        std::string message(int ec) const
        {
            std::string msg;
            strings::ostream os { msg };

            switch (static_cast<parse_errc>(ec)) {
                using enum parse_errc;
                case invalid_char:
                    os << "invalid character";
                    break;
                case no_scheme:
                    os << "no URL scheme";
                    break;
                default:
                    os << "unknown error (" << ec << ')';
                    break;
            }
            return msg;
        }
    } instance;

    return instance;
}

} // namespace stream9::urls

