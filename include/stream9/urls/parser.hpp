#ifndef STREAM9_URL_PARSER_HPP
#define STREAM9_URL_PARSER_HPP

#include "components.hpp"

#include <stream9/string_view.hpp>

#include <system_error>

namespace stream9::urls {

class event_handler;

// functions

components parse(string_view url);

bool parse(string_view url, event_handler&);
bool parse(char const* begin, char const* end, event_handler&);

// types

enum class parse_errc {
    ok = 0,
    invalid_char,
    no_scheme,
};

class event_handler
{
public:
    virtual ~event_handler() {};

    virtual void on_scheme(string_view) {}
    virtual void on_user_info(string_view) {}
    virtual void on_host(string_view) {}
    virtual void on_port(string_view) {}
    virtual void on_path(string_view) {}
    virtual void on_query(string_view) {}
    virtual void on_fragment(string_view) {}

    virtual void on_error(parse_errc) {}
};

std::error_category const& parse_error();

inline std::error_code
make_error_code(parse_errc ec)
{
    return { static_cast<int>(ec), parse_error() };
}

} // namespace stream9::urls

namespace std {

template<>
struct is_error_code_enum<stream9::urls::parse_errc> : true_type {};

} // namespace std

#endif // STREAM9_URL_PARSER_HPP
