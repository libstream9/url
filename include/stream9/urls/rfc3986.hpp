#ifndef STREAM9_RFC3986_HPP
#define STREAM9_RFC3986_HPP

#include <stream9/string_view.hpp>

#include <utility>

namespace stream9::rfc3986 {

using iterator = string_view::iterator;

template<typename T>
concept scanner = requires (T fn, iterator i) {
    { fn(i, i) } -> std::same_as<bool>;
};

// meta
inline constexpr bool
char_(iterator& i, iterator e, char c)
{
    if (i == e || *i != c) return false;

    ++i;

    return true;
}

template<typename... Ts>
inline constexpr bool
char_(iterator& i, iterator e, char c, Ts... rest)
{
    if (char_(i, e, c)) return true;

    return char_(i, e, rest...);
}

inline constexpr auto
char_(char c)
{
    return [=](iterator& i, iterator e) {
        return char_(i, e, c);
    };
}

template<typename... Ts>
constexpr auto
char_(char c, Ts... rest)
{
    return [=](iterator& i, iterator e) {
        return char_(i, e, c, rest...);
    };
}

inline constexpr bool
string_(iterator& i, iterator e, string_view s)
{
    auto [si, se] = s;
    while (i != e && si != se) {
        if (*i != *si) return false;

        ++i; ++si;
    }

    return si == se;
}

inline constexpr auto
string_(string_view s)
{
    return [=](iterator& i, iterator e) {
        return string_(i, e, s);
    };
}

template<scanner T>
inline constexpr bool
repeat(iterator& i, iterator e, int min, int max, T&& fn)
{
    int x = 0;
    for (; x < min; ++x) {
        auto j = i;
        if (!fn(i, e)) {
            i = j;
            return false;
        }
    }

    for (; max < 0 || x < max; ++x) {
        auto j = i;
        if (!fn(i, e)) {
            i = j;
            break;
        }
    }

    return true;
}

template<scanner T>
inline constexpr auto
repeat(int min, int max, T&& fn)
{
    return [=](iterator& i, iterator e) {
        return repeat(i, e, min, max, std::move(fn));
    };
}

template<scanner T>
inline constexpr bool
repeat(iterator& i, iterator e, int n, T&& fn)
{
    int x = 0;
    for (; x < n; ++x) {
        auto j = i;
        if (!fn(i, e)) {
            i = j;
            return false;
        }
    }

    return true;
}

template<scanner T>
inline constexpr auto
repeat(int n, T&& fn)
{
    return [=](iterator& i, iterator e) {
        return repeat(i, e, n, std::move(fn));
    };
}

template<scanner T>
inline constexpr auto
zero_or_more(T&& fn)
{
    return [=](iterator& i, iterator e) {
        while (i != e) {
            auto j = i;
            if (!fn(i, e)) {
                i = j;
                break;
            }
        }

        return true;
    };
}

template<scanner T>
inline constexpr auto
operator*(T&& fn)
{
    return zero_or_more(std::forward<T>(fn));
}

template<scanner T>
inline constexpr auto
one_or_more(T&& fn)
{
    return [=](iterator& i, iterator e) {
        if (!fn(i, e)) return false;

        while (i != e) {
            auto j = i;
            if (!fn(i, e)) {
                i = j;
                break;
            }
        }

        return true;
    };
}

template<scanner T>
inline constexpr auto
operator+(T&& fn)
{
    return one_or_more(std::forward<T>(fn));
}

template<scanner T1, scanner T2>
inline constexpr auto
either(T1&& fn1, T2&& fn2)
{
    return [=](iterator& i, iterator e) {
        auto j = i;
        if (fn1(i, e)) return true;

        i = j;
        if (fn2(i, e)) return true;

        i = j;
        return false;
    };
}

template<typename T1, typename T2>
inline constexpr auto
operator|(T1&& x, T2&& y)
{
    if constexpr (std::same_as<T1, char>) {
        return either(char_(x), std::forward<T2>(y));
    }
    else if constexpr (std::same_as<T2, char>) {
        return either(std::forward<T1>(x), char_(y));
    }
    else if constexpr (std::convertible_to<T1, string_view>) {
        return either(string_(x), std::forward<T2>(y));
    }
    else if constexpr (std::convertible_to<T2, string_view>) {
        return either(std::forward<T1>(x), string_(y));
    }
    else {
        static_assert(scanner<T1> && scanner<T2>);
        return either(std::forward<T1>(x), std::forward<T2>(y));
    }
}

template<scanner T1, scanner T2>
inline constexpr auto
then(T1&& fn1, T2&& fn2)
{
    return [=](iterator& i, iterator e) {
        if (!fn1(i, e)) return false;
        if (!fn2(i, e)) return false;

        return true;
    };
}

template<typename T1, typename T2>
inline constexpr auto
operator>>(T1&& x, T2&& y)
{
    if constexpr (std::same_as<T1, char>) {
        return then(char_(x), std::forward<T2>(y));
    }
    else if constexpr (std::same_as<T2, char>) {
        return then(std::forward<T1>(x), char_(y));
    }
    else if constexpr (std::convertible_to<T1, string_view>) {
        return then(string_(x), std::forward<T2>(y));
    }
    else if constexpr (std::convertible_to<T2, string_view>) {
        return then(std::forward<T1>(x), string_(y));
    }
    else {
        static_assert(scanner<T1> && scanner<T2>);
        return then(std::forward<T1>(x), std::forward<T2>(y));
    }
}

inline constexpr auto
opt(auto&& fn)
{
    return [=](iterator& i, iterator e) {
        auto j = i;

        if (!fn(i, e)) i = j;

        return true;
    };
}

// classifier
inline constexpr bool
is_alpha(char c) noexcept
{
    return ('A' <= c && c <= 'Z')
        || ('a' <= c && c <= 'z');
}

inline constexpr bool
is_digit(char c) noexcept
{
    return ('0' <= c && c <= '9');
}

inline constexpr bool
is_hex_dig(char c) noexcept
{
    return ('0' <= c && c <= '9')
        || ('a' <= c && c <= 'f')
        || ('A' <= c && c <= 'F');
}

inline constexpr bool
is_sub_delim(char c) noexcept
{
    return c == '!'
        || c == '$'
        || c == '&'
        || c == '\''
        || c == '('
        || c == ')'
        || c == '*'
        || c == '+'
        || c == ','
        || c == ';'
        || c == '=';
}

inline constexpr bool
is_gen_delim(char c) noexcept
{
    return c == ':'
        || c == '/'
        || c == '?'
        || c == '#'
        || c == '['
        || c == ']'
        || c == '@';
}

inline constexpr bool
is_reserved(char c) noexcept
{
    return is_gen_delim(c) || is_sub_delim(c);
}

inline constexpr bool
is_unreserved(char c) noexcept
{
    return is_alpha(c)
        || is_digit(c)
        || c == '-'
        || c == '.'
        || c == '_'
        || c == '~';
}

// scanner
#if false
#define define_scanner(N, X) \
inline constexpr auto N = X             // this will explode compiler's memory
#else
#define define_scanner(N, X) \
inline constexpr auto N = \
    [](iterator& i, iterator e) { \
        static constexpr auto fn = X; \
        return fn(i, e); \
    }
#endif

inline constexpr auto alpha =
    [](iterator& i, iterator e) {
        if (i == e) return false;

        if (!is_alpha(*i)) return false;

        ++i;
        return true;
    };

inline constexpr auto digit =
    [](iterator& i, iterator e) {
        if (i == e) return false;

        if (!is_digit(*i)) return false;

        ++i;
        return true;
    };

inline constexpr auto hex_dig =
    [](iterator& i, iterator e) {
        if (i == e) return false;

        if (!is_hex_dig(*i)) return false;

        ++i;
        return true;
    };

inline constexpr auto pct_encoded =
    [](iterator& i, iterator e) {
        if (i == e) return false;

        if (*i != '%') return false;
        ++i;
        if (!is_hex_dig(*i)) return false;
        ++i;
        if (!is_hex_dig(*i)) return false;
        ++i;

        return true;
    };

inline constexpr auto unreserved =
    [](iterator& i, iterator e) {
        if (i == e) return false;

        if (!is_unreserved(*i)) return false;

        ++i;
        return true;
    };

inline constexpr auto sub_delim =
    [](iterator& i, iterator e) {
        if (i == e) return false;

        if (!is_sub_delim(*i)) return false;

        ++i;
        return true;
    };

inline constexpr auto pchar =
    [](iterator& i, iterator e) {
        if (i == e) return false;

        if (unreserved(i, e)) return true;
        if (sub_delim(i, e)) return true;
        if (char_(i, e, ':', '@')) return true;
        if (pct_encoded(i, e)) return true;

        return false;
    };

inline constexpr auto dec_octet =
    [](iterator& i, iterator e) {
        if (i == e) return false;

        if (*i == '0') {
            ++i;
        }
        else if (*i == '1') {
            ++i;
            if (is_digit(*i)) {
                ++i;
                if (is_digit(*i)) {
                    ++i;
                }
            }
        }
        else if (*i == '2') {
            ++i;
            if ('0' <= *i && *i <= '4') {
                ++i;
                if (is_digit(*i)) {
                    ++i;
                }
            }
            else if (*i == '5') {
                ++i;
                if ('0' <= *i && *i <= '5') {
                    ++i;
                }
            }
            else if ('6' <= *i && *i <= '9') {
                ++i;
            }
        }
        else if ('3' <= *i && *i <= '9') {
            ++i;
            if (is_digit(*i)) {
                ++i;
            }
        }
        else {
            return false;
        }

        return true;
    };

inline constexpr auto path_empty =
    [](iterator& i, iterator e) {
        if (i == e) return true;

        auto j = i;
        if (pchar(i, e)) return false;
        i = j;

        return true;
    };

define_scanner(fragment, *(pchar | char_('/', '?')) );

define_scanner(query, *(pchar | char_('/', '?')));

define_scanner(segment, *pchar);
define_scanner(segment_nz, +pchar);
define_scanner(segment_nz_nc,
    +(unreserved | sub_delim | '@' | pct_encoded));

define_scanner(path_abempty, *('/' >> segment));
define_scanner(path_absolute,
    '/' >> opt(segment_nz >> *('/' >> segment)) );
define_scanner(path_noscheme, segment_nz_nc >> *('/' >> segment));
define_scanner(path_rootless, segment_nz >> *('/' >> segment));

define_scanner(path,   path_abempty | path_absolute | path_noscheme
                     | path_rootless | path_empty );

define_scanner(reg_name, *(unreserved | pct_encoded | sub_delim));

define_scanner(ipv4address,    dec_octet >> '.'
                            >> dec_octet >> '.'
                            >> dec_octet >> '.' >> dec_octet );

define_scanner(h16, repeat(1, 4, hex_dig));
define_scanner(ls32, (h16 >> ':' >> h16) | ipv4address);

define_scanner(ipv6address,
                                               (repeat(6, h16 >> ':') >> ls32)
    | (                                 "::" >> repeat(5, h16 >> ':') >> ls32)
    | (opt(h16)                      >> "::" >> repeat(4, h16 >> ':') >> ls32)
    | (opt(repeat(0, 1, h16 >> ':')) >> "::" >> repeat(3, h16 >> ':') >> ls32)
    | (opt(repeat(0, 2, h16 >> ':')) >> "::" >> repeat(2, h16 >> ':') >> ls32)
    | (opt(repeat(0, 3, h16 >> ':')) >> "::" >> h16 >> ':' >> ls32)
    | (opt(repeat(0, 4, h16 >> ':')) >> "::" >> ls32)
    | (opt(repeat(0, 5, h16 >> ':')) >> "::" >> h16)
    | (opt(repeat(0, 6, h16 >> ':')) >> "::")
);

define_scanner(ipvfuture,
    'v' >> +hex_dig >> '.' >> +(unreserved | sub_delim | ':') );
define_scanner(ip_literal, '[' >> (ipv6address | ipvfuture) >> ']');

define_scanner(port, repeat(0, 4, digit));
define_scanner(host, ip_literal | ipv4address | reg_name);
define_scanner(user_info, *(unreserved | pct_encoded | sub_delim | ':'));
define_scanner(authority, opt(user_info >> '@') >> host >> opt(':' >> port));

define_scanner(scheme, alpha >> *(alpha | digit | char_('+', '-', '.')));

define_scanner(relative_part,
    "//" >> authority
            >> (path_abempty | path_absolute | path_noscheme | path_empty) );

define_scanner(relative_ref,
     relative_part >> opt('?' >> query) >> opt('#' >> fragment) );

define_scanner(hier_part,
    "//" >> authority
            >> (path_abempty | path_absolute | path_rootless | path_empty) );

define_scanner(absolute_uri,
    scheme >> ':' >> hier_part >> opt('?' >> query));

define_scanner(uri,
    scheme >> ':' >> hier_part
           >> opt('?' >> query) >> opt('#' >> fragment) );

define_scanner(uri_reference, uri | relative_ref);

} // namespace stream9::rfc3986

#endif // STREAM9_RFC3986_HPP
