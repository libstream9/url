#ifndef STREAM9_URLS_UNESCAPE_HPP
#define STREAM9_URLS_UNESCAPE_HPP

#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

namespace stream9::urls {

string unescape(string_view);

} // namespace stream9::urls

#endif // STREAM9_URLS_UNESCAPE_HPP
