#ifndef STREAM9_URL_URL_COMPONENTS_HPP
#define STREAM9_URL_URL_COMPONENTS_HPP

#include <stream9/string_view.hpp>

namespace stream9::urls {

struct components
{
    string_view scheme = "";
    string_view user_info = "";
    string_view host = "";
    string_view port = "";
    string_view path = "";
    string_view query = "";
    string_view fragment = "";
};

string_view serialize(components const& c);

} // namespace stream9::urls

#endif // STREAM9_URL_URL_COMPONENTS_HPP
