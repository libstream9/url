#ifndef STREAM9_URL_VALIDATOR_HPP
#define STREAM9_URL_VALIDATOR_HPP

#include "components.hpp"

#include <system_error>

namespace stream9::urls {

bool validate(components const&);

enum class validation_errc {
    ok = 0,
    empty_scheme,
    invalid_character,
    premature_end_of_input,
    invalid_ip_literal,
    invalid_ipv6_address,
};

std::error_category const& validation_error();

inline std::error_code
make_error_code(validation_errc ec)
{
    return { static_cast<int>(ec), validation_error() };
}

} // namespace stream9::urls

namespace std {

template<>
struct is_error_code_enum<stream9::urls::validation_errc> : true_type {};

} // namespace std

#endif // STREAM9_URL_VALIDATOR_HPP
