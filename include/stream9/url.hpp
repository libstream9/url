#ifndef STREAM9_URL_URL_HPP
#define STREAM9_URL_URL_HPP

#include "urls/components.hpp"

#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

#include <iosfwd>

namespace stream9 {

class url
{
public:
    using size_type = string::size_type;
    using const_iterator = string::const_iterator;

public:
    // essential
    url(string_view);
    url& operator=(string_view);

    url(url const&) = default;
    url& operator=(url const&) = default;
    url(url&&) = default;
    url& operator=(url&&) = default;

    ~url() = default;

    // accessor
    const_iterator begin() const noexcept { return m_str.begin(); }
    const_iterator end() const noexcept { return m_str.end(); }

    char const* data() const noexcept { return m_str.data(); }
    char const* c_str() const noexcept { return m_str.c_str(); }

    // query
    string_view scheme() const noexcept;
    string_view user_info() const noexcept;
    string_view host() const noexcept;
    string_view port() const noexcept;
    string_view path() const noexcept;
    string_view query() const noexcept;
    string_view fragment() const noexcept;

    urls::components components() const noexcept;

    size_type size() const noexcept { return m_str.size(); }

    // conversion
    operator string_view() const noexcept { return m_str; }

    // equality / ordering
    bool operator==(url const&) const noexcept = default;
    bool operator==(string_view x) const noexcept { return m_str == x; }

    auto operator<=>(url const&) const noexcept = default;
    auto operator<=>(string_view x) const noexcept { return m_str <=> x; }

    // utility
    friend std::ostream& operator<<(std::ostream&, url const&);

private:
    string m_str;
};

} // namespace stream9

#endif // STREAM9_URL_URL_HPP
